<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LogController@index')->name('log');
Route::post('/', 'LogController@store')->name('saveLog');
Route::get('/list', 'LogController@list')->name('list');
Route::post('/list', 'LogController@list')->name('list-post');
Route::get('/summary', 'LogController@summary')->name('summary');
Route::post('/summary', 'LogController@summary')->name('summary-post');