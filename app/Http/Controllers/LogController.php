<?php

namespace App\Http\Controllers;
use App\Country;
use App\FoodType;
use App\LocationType;
use App\Log;
use App\FoodLog;
use Validator;
use DB;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['countries'] = $this->getCountries();
        $data['foodTypes'] = $this->getFoodTypes();
        $data['locationTypes'] = $this->getLocationTypes();
        return view('survey')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $validatedData = $request->validate([
            'email' => 'required|email|max:60',
            'country_id' => 'required',
            'location_type' => 'required',
            'log_date' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required|after:start_time',
            'food_type_id' => 'required',
            'food_count' => 'required|numeric|min:0',
            'duck_count' => 'required|numeric|min:0',
            'location_type_others' => 'required_if:location_type,==,0',
            'food_type_others' => 'required_if:food_type_id,==,0',
            ],[
                'end_time.after' => 'The end time must be after the start time',
                'food_count.required' => 'The food weight field is required.',
                'food_count.numeric' => 'The food weight field must be a number.',
                'food_count.min' => 'The food weight field must greater than zero.',
                'duck_count.required' => 'The duck count field is required.',
                'duck_count.numeric' => 'The duck count field must be a number.',
                'duck_count.min' => 'The duck count field must greater than zero.',
                'location_type_others.required_if' => 'The location type is required.',
                'food_type_others.required_if' => 'The food type is required.',
            ]);
        
        //check if food type exists
        if(isset($request->food_type_others)) {
            $isExist = FoodType::where('name', $request->food_type_others)->count();
            if($isExist <= 0) {
                //save new food type
                $foodType = new FoodType;
                $foodType->name = $request->food_type_others;
                $foodType->save();
            }
        }
        
        //check if location type exists
        if(isset($request->location_type_others)) {
            $isExist = LocationType::where('name', $request->location_type_others)->count();
            if($isExist <= 0) {
                //save new location type
                $locationType = new LocationType;
                $locationType->name = $request->location_type_others;
                $locationType->save();
            }
        }
        $log = new Log;
        $log->email = $request->email;
        $log->country_id = $request->country_id;
        $log->location_type_id = isset($locationType->id) ? $locationType->id : $request->location_type;
        $log->log_date = $request->log_date;
        $log->start_time = $request->start_time;
        $log->end_time = $request->end_time;
        $log->ducks_fed_count = $request->duck_count;
        $log->food_type_id = isset($foodType->id) ? $foodType->id : $request->food_type_id;
        $log->food_count = $request->food_count;
        $log->repeat = isset($request->repeat) ? $request->repeat : 0;
        $log->save();
        // $foodLog = new FoodLog;
        // $foodLog->log_id = $log->id;
        // $foodLog->food_type_id = isset($foodType->id) ? $foodType->id : $request->food_type_id;
        // $foodLog->save();
        return redirect()->route('log')->with('success', 'Log entry successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getCountries()
    {
        $records = Country::orderBy('name', 'asc')->get();
        $result = array();
        foreach($records as $k => $v)
            $result[$v->code] = $v->name;
        return $result;
    }

    private function getFoodTypes()
    {
        $records = FoodType::orderBy('name', 'asc')->get();
        $result = array();
        foreach($records as $k => $v)
            $result[$v->food_type_id] = $v->name;
        return $result;
    }

    private function getLocationTypes()
    {
        $records = LocationType::orderBy('name', 'asc')->get();
        $result = array();
        foreach($records as $k => $v)
            $result[$v->location_type_id] = $v->name;
        return $result;
    }

    public function list(Request $request) {
        if(isset($request->daterange))
        {
            $dates = explode("-", $request->daterange);
            $startDate = date("Y-m-d", strtotime(trim($dates[0])));
            $endDate = date("Y-m-d 23:59:59", strtotime(trim($dates[1])));
            session(['startDate' => $startDate]);
            session(['endDate' => $endDate]);
        }
        else if(session('startDate')!= null)
        {
            $startDate = date("Y-m-d", strtotime(session('startDate')));
            $endDate = date("Y-m-d 23:59:59", strtotime(session('endDate')));
        }
        else
        {
            $startDate = date("Y-m-01");
            $endDate = date("Y-m-d 23:59:59", mktime(0, 0, 0,date("m")+1,0,date("Y")));
        }
        if(!empty($request->btnSummary))
        {
            return redirect()->route('summary');
        }
        $data['records'] = Log::orderBy('log_date', 'DESC')
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->get();
        $data['date_range'] = date("m/d/Y", strtotime($startDate))." - ".date("m/d/Y", strtotime($endDate));
        $data['countries'] = $this->getCountries();
        $data['foodTypes'] = $this->getFoodTypes();
        $data['locationTypes'] = $this->getLocationTypes();
        return view('list')->with($data);
    }

    public function summary(Request $request) {
        if(isset($request->daterange))
        {
            $dates = explode("-", $request->daterange);
            $startDate = date("Y-m-d", strtotime(trim($dates[0])));
            $endDate = date("Y-m-d 23:59:59", strtotime(trim($dates[1])));
            session(['startDate' => $startDate]);
            session(['endDate' => $endDate]);
        }
        else if(session('startDate')!= null)
        {
            $startDate = date("Y-m-d", strtotime(session('startDate')));
            $endDate = date("Y-m-d 23:59:59", strtotime(session('endDate')));
        }
        else
        {
            $startDate = date("Y-m-01");
            $endDate = date("Y-m-d 23:59:59", mktime(0, 0, 0,date("m")+1,0,date("Y")));
        }
        
        $data['date_range'] = date("m/d/Y", strtotime($startDate))." - ".date("m/d/Y", strtotime($endDate));
        $countries = $this->getCountries();
        $foodTypes = $this->getFoodTypes();
        $locationTypes = $this->getLocationTypes();
        // for($i = 0; $i < 24; $i++) {
        $hourDisplay[0] = "12:00 AM";
        $hourDisplay[1] = "01:00 AM";
        $hourDisplay[2] = "02:00 AM";
        $hourDisplay[3] = "03:00 AM";
        $hourDisplay[4] = "04:00 AM";
        $hourDisplay[5] = "05:00 AM";
        $hourDisplay[6] = "06:00 AM";
        $hourDisplay[7] = "07:00 AM";
        $hourDisplay[8] = "08:00 AM";
        $hourDisplay[9] = "09:00 AM";
        $hourDisplay[10] = "10:00 AM";
        $hourDisplay[11] = "11:00 AM";
        $hourDisplay[12] = "12:00 PM";
        $hourDisplay[13] = "1:00 PM";
        $hourDisplay[14] = "2:00 PM";
        $hourDisplay[15] = "3:00 PM";
        $hourDisplay[16] = "4:00 PM";
        $hourDisplay[17] = "5:00 PM";
        $hourDisplay[18] = "6:00 PM";
        $hourDisplay[19] = "7:00 PM";
        $hourDisplay[20] = "8:00 PM";
        $hourDisplay[21] = "9:00 PM";
        $hourDisplay[22] = "10:00 PM";
        $hourDisplay[23] = "11:00 PM";
        // }
        //Graph 1 Time over frequencies
        $graph1 = Log::select(DB::raw('count(*) as count, HOUR(start_time) as hour'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('hour')
                        ->get();
        $result1[] = ['Time','Count'];
        $record = array();
        foreach ($graph1 as $key => $value)
            $record[$hourDisplay[(int) $value->hour]] = $value->count;
        foreach ($hourDisplay as $key => $value){
            $count = isset($record[$value]) ? $record[$value] : 0;
            $result1[] = [$value, $count];
        }
        //Graph 2 Time over total number of duck count
        $graph2 = Log::select(DB::raw('sum(ducks_fed_count) as count, HOUR(start_time) as hour'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('hour')
                        ->get();
        $result2[] = ['Time','Count'];
        $record = array();
        foreach ($graph2 as $key => $value)
            $record[$hourDisplay[(int) $value->hour]] = (int) $value->count;
        foreach ($hourDisplay as $key => $value){
            $count = isset($record[$value]) ? $record[$value] : 0;
            $result2[] = [$value, $count];
        }
        //Graph 3 and 4 food type frequency and total weight
        $graph3 = Log::select(DB::raw('sum(food_count) as food_count, food_type_id, count(food_type_id) as count'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('food_type_id')
                        ->get();
        $result3[] = ['Food Type','Frequency'];
        $result4[] = ['Food Type','Total'];
        $record = array();
        foreach ($graph3 as $key => $value) {
            $record[$value->food_type_id]['frequency']= (int) $value->count;
            $record[$value->food_type_id]['total']= (int) $value->food_count;
        }
            
        foreach ($foodTypes as $key => $value){
            $frequency = isset($record[$key]['frequency']) ? $record[$key]['frequency'] : 0;
            $total = isset($record[$key]['total']) ? $record[$key]['total'] : 0;
            $result3[] = [$value, $frequency];
            $result4[] = [$value, $total];
        }
        //Graph 5 Location type frequency 
        $graph5 = Log::select(DB::raw('count(*) as count, location_type_id'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('location_type_id')
                        ->get();
        $result5[] = ['Location Type','Frequency'];
        $record = array();
        foreach ($graph5 as $key => $value) {
            $record[$value->location_type_id] = (int) $value->count;
        }

        foreach ($locationTypes as $key => $value){
            $frequency = isset($record[$key]) ? $record[$key] : 0;
            $result5[] = [$value, $frequency];
        }
        //Graph 6 Total number of ducks fed per location type
        $graph6 = Log::select(DB::raw('sum(ducks_fed_count) as count, location_type_id'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('location_type_id')
                        ->get();
        $result6[] = ['Location Type','Total Ducks'];
        $record = array();
        foreach ($graph6 as $key => $value) {
            $record[$value->location_type_id] = (int) $value->count;
        }

        foreach ($locationTypes as $key => $value){
            $count = isset($record[$key]) ? $record[$key] : 0;
            $result6[] = [$value, $count];
        }
        //Graph 7 Total number of ducks fed per country
        $graph7 = Log::select(DB::raw('sum(ducks_fed_count) as count, country_id'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('country_id')
                        ->get();
        $result7[] = ['Country','Total Ducks'];
        $record = array();
        foreach ($graph7 as $key => $value) {
            $result7[] = [$countries[$value->country_id], (int) $value->count];
        }
        //graph 8
        $graph8 = Log::select(DB::raw('sum(ducks_fed_count) as duck_count, sum(food_count) as food_count, HOUR(start_time) as hour'))
                        ->whereBetween('log_date',[$startDate, $endDate])
                        ->groupBy('hour')
                        ->get();
        $result8[] = ['Time','Duck Count','Food Count'];
        $record = array();
        foreach ($graph8 as $key => $value) {
            $record[$hourDisplay[(int) $value->hour]]['food'] = $value->food_count;
            $record[$hourDisplay[(int) $value->hour]]['duck'] = $value->duck_count;
        }
        foreach ($hourDisplay as $key => $value){
            $duck = isset($record[$value]['duck']) ? (int) $record[$value]['duck'] : 0;
            $food = isset($record[$value]['food']) ? (int) $record[$value]['food'] : 0;
            $result8[] = [$value, $duck, $food];
        }

        $data['graph1'] = json_encode($result1);
        $data['graph2'] = json_encode($result2);
        $data['graph3'] = json_encode($result3);
        $data['graph4'] = json_encode($result4);
        $data['graph5'] = json_encode($result5);
        $data['graph6'] = json_encode($result6);
        $data['graph7'] = json_encode($result7);
        $data['graph8'] = json_encode($result8);
        return view('summary')->with($data);
    }
}
