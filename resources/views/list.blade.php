<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duck Feeding Logs - List</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" />
        <style>
            body, html {
                height:80%;
                padding:10px 10px 0 10px;
            }
            .error-message {
                width: 100%;
                margin-top: 0.25rem;
                font-size: 80%;
                color: #f86c6b;
            }
            /* .container {
                width: 90%;
            } */
        </style>
    </head>
    <body>
        <section>
            <!-- <div class="container"> -->
                <div class="card">
                    <div class="card-header">
                        <h3>Duck Feeding Logs</h3>
                    </div>
                    <form name="list" method="post" action=" {{ route('list-post') }}">
                        @csrf    
                        <div class="row" style="padding: 10px 0;">
                            <div class="form-group col-lg-11 col-md-12 row" style="margin: 0 auto;">
                                <label class="col-sm-12 col-md-3 col-lg-2 padding-0 col-form-label">Log Date range:</label>
                                <label class="col-sm-12 col-md-9 col-lg-10 padding-0 col-form-label">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="daterangepicker" name="daterange" value="{{ $date_range }}">
                                        <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit" value="Search" name="btnSearch">SEARCH</button>
                                            <button class="btn btn-success" type="submit" value="Summary" name="btnSummary">SUMMARY</button>
                                        </span>
                                    </div> 
                                </label>
                            </div>
                        </div>
                    </form>
                    <div class="card-body row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped datatable dataTable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Email Address</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Country</th>
                                            <th>Location Type</th>
                                            <th>Food Type</th>
                                            <th>Food Count (in grams)</th>
                                            <th>Duck Count</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($records as $k => $record)
                                            <tr>
                                                <td>{{ $record->email }}</td>
                                                <td>{{ date("F d, Y", strtotime($record->log_date)) }}</td>
                                                <td>{{ date("h:i A", strtotime($record->start_time)).' - '.date("h:i A", strtotime($record->end_time)) }}</td>
                                                <td>{{ isset($countries[$record->country_id]) ? $countries[$record->country_id] : '' }}</td>
                                                <td>{{ isset($locationTypes[$record->location_type_id]) ? $locationTypes[$record->location_type_id] : '' }}</td>
                                                <td>{{ isset($foodTypes[$record->location_type_id]) ? $foodTypes[$record->location_type_id] : '' }}</td>
                                                <td>{{ $record->food_count }}</td>
                                                <td>{{ $record->ducks_fed_count }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> -->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
    <script>
        $(function () {
            $('.datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'print'
                ],
                scrollX: true,
            });
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
        });
    </script>
</html>