<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duck Feeding Logs - Summary</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <style>
            body, html {
                height:80%;
                padding:10px 10px 0 10px;
            }
            .error-message {
                width: 100%;
                margin-top: 0.25rem;
                font-size: 80%;
                color: #f86c6b;
            }
            /* .container {
                width: 90%;
            } */
        </style>
    </head>
    <body>
        <section>
            <!-- <div class="container"> -->
                <div class="card">
                    <div class="card-header">
                        <h3>Duck Feeding Log Summary</h3>
                    </div>
                    <form name="list" method="post" action=" {{ route('summary-post') }}">
                        @csrf    
                        <div class="row" style="padding: 10px 0;">
                            <div class="form-group col-lg-11 col-md-12 row" style="margin: 0 auto;">
                                <label class="col-sm-12 col-md-3 col-lg-2 padding-0 col-form-label">Log Date range:</label>
                                <label class="col-sm-12 col-md-9 col-lg-10 padding-0 col-form-label">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="daterangepicker" name="daterange" value="{{ $date_range }}">
                                        <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit" value="Search" name="btnSearch">Display</button>
                                            <button class="btn btn-secondary" type="submit" value="Summary" name="btnSummary">Back to List</button>
                                        </span>
                                    </div> 
                                </label>
                            </div>
                        </div>
                    </form>
                    <div class="card-body row">
                        <div id="chart1" style="height: 500px" class="col-sm-12"></div>
                        <div id="chart2" style="height: 500px" class="col-sm-12"></div>
                        <div id="chart3" style="height: 500px" class="col-sm-12"></div>
                        <div id="chart4" style="height: 500px" class="col-sm-12"></div>
                        <div id="chart5" style="height: 500px" class="col-sm-6"></div>
                        <div id="chart6" style="height: 500px" class="col-sm-6"></div>
                        <div id="chart7" style="height: 500px" class="col-sm-12"></div>
                        <div id="chart8" style="height: 500px" class="col-sm-12"></div>
                    </div>
                </div>
            <!-- </div> -->
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        $(function () {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
        });
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        var graph1 = <?php echo $graph1; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart1);
        function drawChart1() {
            var data = google.visualization.arrayToDataTable(graph1);
            var options = {
                title: 'Number of times the ducks are fed based on time',
                curveType: 'function',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('chart1'));
            chart.draw(data, options);
        }
        var graph2 = <?php echo $graph2; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart2);
        function drawChart2() {
            var data = google.visualization.arrayToDataTable(graph2);
            var options = {
                title: 'Total number of ducks are fed based on time',
                curveType: 'function',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('chart2'));
            chart.draw(data, options);
        }
        var graph3 = <?php echo $graph3; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart3);
        function drawChart3() {
            var data = google.visualization.arrayToDataTable(graph3);
            var options = {
                title: 'Number of times the ducks are fed based on food type',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart3'));
            chart.draw(data, options);
        }
        var graph4 = <?php echo $graph4; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart4);
        function drawChart4() {
            var data = google.visualization.arrayToDataTable(graph4);
            var options = {
                title: 'Total number of food the ducks fed (in grams) based on food type',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart4'));
            chart.draw(data, options);
        }
        var graph5 = <?php echo $graph5; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart5);
        function drawChart5() {
            var data = google.visualization.arrayToDataTable(graph5);
            var options = {
                title: 'Number of times the ducks are fed based on location type',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.PieChart(document.getElementById('chart5'));
            chart.draw(data, options);
        }
        var graph6 = <?php echo $graph6; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart6);
        function drawChart6() {
            var data = google.visualization.arrayToDataTable(graph6);
            var options = {
                title: 'Total number of ducks fed based on location type',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.PieChart(document.getElementById('chart6'));
            chart.draw(data, options);
        }
        var graph7 = <?php echo $graph7; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart7);
        function drawChart7() {
            var data = google.visualization.arrayToDataTable(graph7);
            var options = {
                title: 'Total number of ducks fed based per country',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('chart7'));
            chart.draw(data, options);
        }
        var graph8 = <?php echo $graph8; ?>;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart8);
        function drawChart8() {
            var data = google.visualization.arrayToDataTable(graph8);
            var options = {
                title: 'Total number of ducks fed and total food given per hour',
                legend: { position: 'bottom' }
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart8'));
            chart.draw(data, options);
        }
    </script>
</html>