<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duck Feeding Logs</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body, html {
                height:80%;
                padding-top:10px;
            }
            .error-message {
                width: 100%;
                margin-top: 0.25rem;
                font-size: 80%;
                color: #f86c6b;
            }
        </style>
    </head>
    <body>
        <!-- Survey form -->
        <section>
            <div class="container">
                @if(session('success'))
                <div class="alert alert-success" role="alert">
                    <h3>{{session('success')}}</h3>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3>Duck Feeding Log</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action=" {{ route('saveLog')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> Who fed the ducks (email address)? </label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" id="inputEmailAddress" placeholder="Email Address" name="email" value="{!! old('email') !!}" >
                                    @if ($errors->has('email'))
                                        <em class="error-message"><strong>{{ $errors->first('email') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> What is the current location of the ducks?  </label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="country_id">
                                    @foreach ($countries as $countryCode => $countryName)
                                        <option value={{ $countryCode }} {{ (old("country_id") == $countryCode ? "selected":"") }}>{{ $countryName }}</option>
                                    @endforeach
                                    </select>
                                    @if ($errors->has('country_id'))
                                            <em class="error-message"><strong>{{ $errors->first('country_id') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> Where the ducks are fed? </label>
                                <div class="col-sm-8 form-inline">
                                    <select class="form-control" name="location_type" id="location_type">
                                    @foreach ($locationTypes as $locationId => $locationName)
                                        <option value={{ $locationId }} {{ (old("location_type") == $locationId ? "selected":"") }}>{{ $locationName }}</option>
                                    @endforeach
                                    <option value=0 {{ ((old("location_type") != '') && (old("location_type") == 0) ? "selected":"") }}>Others</option>
                                    </select>
                                    <div id="location_type_others" style="padding-left:20px;">
                                        @if((old('location_type') != '') && old('location_type') == 0)
                                            <input type="text" class="form-control" id="inputOtherLocation" placeholder="Please type here" name="location_type_others" value="{!! old('location_type_others') !!}">
                                            @if ($errors->has('location_type_others'))
                                                <em class="error-message"><strong>{{ $errors->first('location_type_others') }}</strong></em>
                                            @endif
                                        @endif
                                    </div>
                                    @if ($errors->has('locationId'))
                                            <em class="error-message"><strong>{{ $errors->first('country_locationIdid') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> What date the ducks are fed? </label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="inputLogDate" placeholder="Date" name="log_date" value="{!! old('log_date', date('Y-m-d')) !!}" >
                                    @if ($errors->has('log_date'))
                                            <em class="error-message"><strong>{{ $errors->first('log_date') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> What time the ducks are fed? </label>
                                <div class="col-sm-3">
                                    <input type="time" class="form-control" name="start_time" value="{!! old('start_time', date('H:i')) !!}">
                                    @if ($errors->has('start_time'))
                                            <em class="error-message"><strong>{{ $errors->first('start_time') }}</strong></em>
                                    @endif
                                </div>
                                <label class="col-md-1 col-form-label text-center"> - </label>
                                <div class="col-sm-3">
                                    <input type="time" class="form-control" name="end_time" value="{!! old('end_time', date('H:i')) !!}">
                                    @if ($errors->has('end_time'))
                                            <em class="error-message"><strong>{{ $errors->first('end_time') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> What kind of food the ducks are fed? </label>
                                <div class="col-sm-8 form-inline">
                                    <!-- <div class="form-check-inline"> -->
                                        <select class="form-control" name="food_type_id" id="food_type">
                                            @foreach ($foodTypes as $foodId => $foodName)
                                                <option value={{ $foodId }} {{ (old("food_type_id") == $foodId ? "selected":"") }}>{{ $foodName }}</option>
                                            @endforeach
                                            <option value=0 {{ ((old("food_type_id") != '') && (old("food_type_id") == 0) ? "selected":"") }}>Others</option>
                                        </select>
                                        <div id="food_type_others" style="padding-left:20px;">
                                            @if((old("food_type_id") != '') && (old("food_type_id") == 0))
                                                <input type="text" class="form-control" id="inputOtherLocation" placeholder="Please type here" name="food_type_others" value="{!! old('food_type_others') !!}">
                                                @if ($errors->has('food_type_others'))
                                                    <em class="error-message"><strong>{{ $errors->first('food_type_others') }}</strong></em>
                                                @endif
                                            @endif
                                        </div>
                                        @if ($errors->has('food_type_id'))
                                            <em class="error-message"><strong>{{ $errors->first('food_type_id') }}</strong></em>
                                        @endif
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> How much food the ducks are fed? </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" placeholder="food weight (in grams)" name="food_count" value="{!! old('food_count') !!}" >
                                    @if ($errors->has('food_count'))
                                            <em class="error-message"><strong>{{ $errors->first('food_count') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"><span class="text-danger">*</span> Number of ducks are fed? </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" placeholder="number of ducks" name="duck_count" value="{!! old('duck_count') !!}" >
                                    @if ($errors->has('duck_count'))
                                            <em class="error-message"><strong>{{ $errors->first('duck_count') }}</strong></em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label" for="repeat">Kindly repeat this entry every day</label>
                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline mr1">
                                        <input class="form-check-input" type="checkbox" value="1" name="repeat" id="repeat">
                                        <!-- <label class="form-check-label" for="repeat">I do this everyday.</label> -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center" style="padding-top:20px;">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-secondary" onclick="window.location='';">Cancel</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <small>Fields with <span class="text-danger">*</span> are required.</small>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
    $(document).on("change", "#location_type", function () {
        var type = $("#location_type").val();
        if(type == 0){ //means others
            var string = '<input type="text" class="form-control" id="inputOtherLocation" placeholder="Please type here" name="location_type_others" value="{!! old('location_type_others') !!}">@if ($errors->has('location_type_others'))<em class="error-message"><strong>{{ $errors->first('location_type_others') }}</strong></em>@endif';         
            $("#location_type_others").html(string);
        } else {
            $("#location_type_others").html('');
        } 
    });
    $(document).on("change", "#food_type", function () {
        var type = $("#food_type").val();
        if(type == 0) { //means others
            var string = '<input type="text" class="form-control" id="inputOtherLocation" placeholder="Please type here" name="food_type_others" value="{!! old('food_type_others') !!}"> @if ($errors->has('food_type_others'))<em class="error-message"><strong>{{ $errors->first('food_type_others') }}</strong></em>@endif';         
            $("#food_type_others").html(string);
        } else {
            $("#food_type_others").html('');
        } 
    });
    </script>
</html>