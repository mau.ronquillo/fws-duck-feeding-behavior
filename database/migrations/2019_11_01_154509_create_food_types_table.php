<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_types', function (Blueprint $table) {
            $table->bigIncrements('food_type_id');
            $table->string('name', 200);
            $table->timestamps();
        });
        DB::table('food_types')->insert(
            array('name' => 'Cracked corn'),
            array('name' => 'Wheat, barley or similar grains'),
            array('name' => 'Oats'),
            array('name' => 'Rice'),
            array('name' => 'Milo seed'),
            array('name' => 'Bird seed'),
            array('name' => 'Grapes'),
            array('name' => 'Nut hearts or pieces'),
            array('name' => 'Frozen peas or corn'),
            array('name' => 'Earthworms'),
            array('name' => 'Mealworms'),
            array('name' => 'Chopped lettuce or other greens and salad mixes'),
            array('name' => 'Vegetable trimmings or peels')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_types');
    }
}
