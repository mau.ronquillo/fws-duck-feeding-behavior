<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_types', function (Blueprint $table) {
            $table->bigIncrements('location_type_id');
            $table->string('name', 100);
            $table->timestamps();
        });
        DB::table('location_types')->insert(
            array('name' => 'Park'),
            array('name' => 'Farm'),
            array('name' => 'Pond'),
            array('name' => 'House (Pet)')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_types');
    }
}
