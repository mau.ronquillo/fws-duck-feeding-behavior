<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('log_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('email', 60);
            $table->string('country_id', 3);
            $table->integer('location_type_id');
            $table->integer('food_type_id');
            $table->integer('food_count');
            $table->integer('ducks_fed_count');
            $table->boolean('repeat')->comment("1-execute CRON to duplicate this entry everyday; 0-don't execute");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
